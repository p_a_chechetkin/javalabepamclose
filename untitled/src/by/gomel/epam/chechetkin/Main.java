package by.gomel.epam.chechetkin;


import by.gomel.epam.chechetkin.calculator.RPN;
import by.gomel.epam.chechetkin.exchanger.Exchange;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args[0].equals("1")) {

            System.out.println(args[1] + " = " + RPN.calculate(args[1]));

        } else if (args[0].equals("2") && args.length > 2) {
            if (Integer.parseInt(args[1])<0) throw new Exception("Число не может быть отрицательным");
            String[] coins = new String[args.length-2];
            System.arraycopy(args,2,coins,0,coins.length);
            Exchange.printAllExchanges(args[1], coins);

        } else {
            System.out.println("No");
        }
    }
}
