package by.gomel.epam.chechetkin.calculator;

import java.util.Stack;

public class RPN {

    static public double calculate(String input) {
        String output = getExpression(input);
        return counting(output);
    }


    static private String getExpression(String input) {
        StringBuilder output = new StringBuilder();
        Stack<Character> operStack = new Stack<>();
        for (int i = 0; i < input.length(); i++) {
            if (isDelimeter(input.toCharArray()[i]))
                continue;

            if (Character.isDigit(input.toCharArray()[i])) {
                while (!isDelimeter(input.toCharArray()[i]) && !isOperator(input.toCharArray()[i])) {
                    output.append(input.toCharArray()[i]);
                    i++;
                    if (i == input.length()) break;
                }
                output.append(" ");
                i--;
            }

            if (isOperator(input.toCharArray()[i])) {
                if (input.toCharArray()[i] == '(')
                    operStack.push(input.toCharArray()[i]);
                else if (input.toCharArray()[i] == ')') {
                    Character s = operStack.pop();
                    while (s != '(') {
                        output.append(s.toString()).append(' ');
                        s = operStack.pop();
                    }
                } else {
                    if (!operStack.empty())
                        if (getPriority(input.toCharArray()[i]) <= getPriority(operStack.peek()))
                            output.append(operStack.pop().toString()).append(" ");
                    operStack.push(input.toCharArray()[i]);
                }
            }
        }

        while (!operStack.empty())
            output.append(operStack.pop()).append(" ");
        return output.toString();
    }

    static private double counting(String input) {
        double result = 0;
        Stack<Double> temp = new Stack<>();

        for (int i = 0; i < input.length(); i++) {

            if (Character.isDigit(input.toCharArray()[i])) {
                StringBuilder a = new StringBuilder();

                while (!isDelimeter(input.toCharArray()[i]) && !isOperator(input.toCharArray()[i])) //Пока не разделитель
                {
                    a.append(input.toCharArray()[i]);
                    i++;
                    if (i == input.length()) break;
                }
                temp.push(Double.parseDouble(a.toString()));
                i--;
            } else if (isOperator(input.toCharArray()[i])) {
                double a = temp.pop();
                double b = temp.pop();

                switch (input.toCharArray()[i]) {
                    case '+':
                        result = b + a;
                        break;
                    case '-':
                        result = b - a;
                        break;
                    case '*':
                        result = b * a;
                        break;
                    case '/':
                        result = b / a;
                        break;
                    case '^':
                        result = (Math.pow(b, a));
                        break;
                }
                temp.push(result);
            }
        }
        return temp.peek();
    }

    static private boolean isDelimeter(char currentChar) {
        return (" =".indexOf(currentChar) != -1);
    }

    static private boolean isOperator(char currentChar) {
        return ("+-/*^()".indexOf(currentChar) != -1);
    }

    static private byte getPriority(char s) {
        switch (s) {
            case '(':
                return 0;
            case ')':
                return 1;
            case '+':
                return 2;
            case '-':
                return 3;
            case '*':
                return 4;
            case '/':
                return 4;
            case '^':
                return 5;
            default:
                return 6;
        }
    }
}
