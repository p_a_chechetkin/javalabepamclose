package by.gomel.epam.chechetkin.exchanger;


import java.util.Arrays;

public class Exchange {

    public static void printAllExchanges(String sumForExchangeString, String[] denominationCoinsString) throws Exception {
        int sumForExchange = Integer.parseInt(sumForExchangeString);

        int[] coins = getDenominationCoins(denominationCoinsString);
        Arrays.sort(coins);
        WaysGroup[][] waysGroups = new WaysGroup[coins.length][sumForExchange + 1];
        waysGroups[0][0] = new WaysGroup();
        waysGroups[0][0].add(new Way());
        for (int i = 0; i < sumForExchange; i++) {
            for (int j = 0; j < coins.length; j++) {
                for (int k = j; k < coins.length; k++) {
                    if (i + coins[k] <= sumForExchange) {
                        if (waysGroups[k][i + coins[k]] == null) {
                            waysGroups[k][i + coins[k]] = new WaysGroup();
                        }
                        waysGroups[k][i + coins[k]].add(waysGroups[j][i]);
                    }
                }
                if (i + coins[j] <= sumForExchange) {
                    waysGroups[j][i + coins[j]].add(coins[j]);
                }
                waysGroups[j][i] = null;
            }
        }
        WaysGroup result = new WaysGroup();
        for (int i = 0; i < coins.length; i++) {
            result.add(waysGroups[i][sumForExchange]);
        }

        result.print();

    }

    private static int[] getDenominationCoins(String[] denominationCoinsString) throws Exception {
        int[] denominationCoins = new int[denominationCoinsString.length];
        for (int i = 0; i < denominationCoins.length; i++) {
            denominationCoins[i] = Integer.parseInt(denominationCoinsString[i]);
            for (int j = 0; j < i; j++) {
                if (denominationCoins[j] == denominationCoins[i])
                    throw new Exception("Нельзя что бы номиналы повторялись");
            }
        }

        return denominationCoins;
    }
}
